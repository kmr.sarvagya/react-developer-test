import React, { useState, useEffect } from 'react';
import axios from "axios";

import Grid from './components/Grid';
import Table from './components/Table';

function App() {
    const [data, setData] = useState();
    const [select, setSelect] = useState();

    useEffect(() => {
        axios("https://jsonplaceholder.typicode.com/photos")
            .then(resp => setData(resp.data.slice(0, 4)))
            .then(resp => resp.map(i => i = {...i, compare: false}))
            .catch(err => err.message);
    }, []);
    useEffect(() => {
        if (data) setSelect(data.filter(x => x.compare === true));
    }, [data])

    function handleClick(id) {
        setData(curr => {
            return curr.map(item => {
              if (item.id === id) return { ...item, compare: !item.compare };
              return item;
            });
        });
    }

    return (
        <>
            <h2 style={{textAlign: "center"}}>Photo Listing</h2>
            <div style={{display:"flex", justifyContent:"space-around", textAlign:"center"}}>
                {data ? 
                    data.map(i => <Grid click={handleClick} key={i.id} id={i.id} url={i.thumbnailUrl} title={i.title} compare={i.compare} />) 
                    : "Loading..." }
            </div>
            <table style={{border: "1px solid black", borderCollapse: "collapse", width:"100%", margin:"70px auto"}}>
                <caption>COMPARISON TABLE</caption>
                <thead>
                    <tr>
                        <th>PHOTO</th>
                        <th>ID</th>
                        <th>URL</th>
                        <th>TITLE</th>
                    </tr>
                </thead>
                <tbody>
                    {select ? 
                        select.map(i => <Table key={i.id} id={i.id} url={i.thumbnailUrl} title={i.title} />)
                        : "" }
                </tbody>
            </table>
        </>
    )
}

export default App
