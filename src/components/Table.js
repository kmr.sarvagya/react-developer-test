import React from 'react'

function Table({id, title, url}) {
    const style = {
        border: "1px solid black",
        borderCollapse: "collapse",
        padding: 8
    }

    return (
        <tr>
            <td style={style}><img src={url} alt="" /></td>
            <td style={style}>{id}</td>
            <td style={style}>{url}</td>
            <td style={style}>{title}</td>
        </tr>
    )
}

export default Table
