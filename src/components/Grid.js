import React from 'react'

function Grid({click, compare, id, title, url}) {
    const handleClick = (e) => click(id);

    return (
        <div style={{border:"solid 2px black", padding:3}}>
            <img src={url} alt="" />
            <h5>{title}</h5>
            <h5>{id}</h5>
            <button onClick={handleClick}>{compare ? "Remove" : "Compare"}</button>
        </div>
    )
}

export default Grid
